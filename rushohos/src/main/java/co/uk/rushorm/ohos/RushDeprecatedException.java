package co.uk.rushorm.ohos;

/**
 * Created by stuartc on 27/01/2017.
 */

public class RushDeprecatedException extends RuntimeException {

    public RushDeprecatedException(String message) {
        super(message);
    }

}
